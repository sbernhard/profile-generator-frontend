import { ColorLayer } from "./ColorLayer";
import { LayerDrawer } from "./Layer";
import { Ctx } from "../clientGenerator";

export type TextCircleLayer = {
  type: "TEXT_CIRCLE";
  text: string;
  fontSize: number;
  font: string;
  angle: number;
  center: number;
  radius: number;
  facingInward: boolean;
} & ColorLayer;

export const drawTextCircle: LayerDrawer<TextCircleLayer> = (
  ctx,
  { text, angle, center, radius, font, fontSize, facingInward, color }
) => {
  ctx.save();
  ctx.fillStyle = color;
  ctx.font = `${fontSize}px ${font}`;
  ctx.textBaseline = "middle";
  ctx.textAlign = "center";

  if (facingInward) {
    radius = radius * -1;
  } else {
    angle += Math.PI;
  }

  const startAngle: number = calculateStartAngle(ctx, angle, text, radius);

  ctx.translate(center, center);
  ctx.rotate(startAngle);

  for (let i = 0; i < text.length; i++) {
    const char = text[i];
    const charWidth: number = ctx.measureText(char).width;
    const charAngle: number = charWidth / radius;
    ctx.rotate(-charAngle / 2);
    ctx.fillText(char, 0, radius);
    ctx.rotate(-charAngle / 2);
  }

  ctx.restore();
};

function calculateStartAngle(
  ctx: Ctx,
  startAngle: number,
  text: string,
  radius: number
): number {
  const textWidth = ctx.measureText(text).width;
  const textAngle = textWidth / radius;
  return startAngle + textAngle / 2;
}
